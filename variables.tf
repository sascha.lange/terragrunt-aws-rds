variable "region" {
    description = "Region for resources"
    type = string
}

variable "environment" {
    description = "Environment type"
    type        = string
}

variable "product" {
    description = "Product name"
    type        = string
}

variable "engine" {
    description = "Database engine to use"
    type        = string
    default     = "mysql"
}

variable "major_engine_version" {
    description = "Database engine version to use"
    type        = string
    default     = ""
}

variable "engine_version" {
    description = "Database engine version to use"
    type        = string
    default     = ""
}

variable "instance_class" {
    description = "Instance types to use"
    type        = string
    default     = "db.t2.micro"
}

variable "port" {
    description = "Database port to use"
    type        = string
    default     = ""
}

variable "config" {
    description = "Preset of configuration defaults"
    type        = map
}

variable "maintenance_window" {
    description = "Maintenance window"
    type        = string
    default     = "Mon:00:00-Mon:03:00"
}

variable "backup_window" {
    description = "Backup window"
    type        = string
    default     = "03:00-06:00"
}

variable "db_master_user" {
    description = "Username for master DB user"
    type        = string
    default     = "root"
}

variable "deletion_protection" {
    description = "Database deletion protection"
    type        = bool
    default     = true
}

variable "skip_final_snapshot" {
    description = "Determines whether a final DB snapshot is created before the DB instance is deleted"
    type        = bool
    default     = false
}

variable "family" {
    description = "The family of the DB parameter group"
    type        = string
    default     = ""
}

variable "allocated_storage" {
    description = "Databaes storage configuration"
    type        = number
}

variable "backup_retention_period" {
    description = "Days to retain backups"
    type        = number
    default     = 7
}

variable "multi_az" {
    description = "Specifies if the RDS instance is multi-AZ"
    type        = bool
    default     = false
}

variable "replica_count" {
    description = "Number of read replicas, max 5"
    type        = number
    default     = 0
}

variable "apply_immediately" {
    description = "Apply modification immediatly"
    type        = bool
    default     = false
}

variable "parameters" {
    description = "A list of DB parameters (map) to apply"
    type        = list(map(string))
    default     = []
}

variable "options" {
    description = "A list of Options to apply"
    type        = any
    default     = []
}

variable "storage_encrypted" {
    description = "Specifies whether the DB instance is encrypted"
    type        = bool
    default     = false
}

variable "parameter_store_key_prefix" {
    description = "Key prefix for items created in the paramter store"
    type        = string
}
