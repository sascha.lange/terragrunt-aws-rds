config = {
  "mysql_port" = 3306
  "mysql_major_engine_version" = "8.0"
  "mysql_engine_version" = "8.0.16"
  "mysql_family" = "8.0"
  "postgres_port" = 5432
  "postgres_major_engine_version" = "11.4"
  "postgres_engine_version" = "11.4"
  "postgres_family" = "11"
}
