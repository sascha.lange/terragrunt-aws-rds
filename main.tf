terraform {
  required_version = ">= 0.12, < 0.13"
  backend "s3" {}
}

provider "aws" {
  version = "~> 2.8"
  region  = var.region
}

# Variable common to all instances
locals {
  engine                = var.engine
  engine_version        = var.engine_version != "" ? var.engine_version : "${lookup(var.config, "${var.engine}_engine_version")}"
  instance_class        = var.instance_class
  allocated_storage     = var.allocated_storage
  max_allocated_storage = var.allocated_storage * 2
  storage_encrypted     = var.storage_encrypted
  port                  = var.port != "" ? var.port : "${lookup(var.config, "${var.engine}_port")}"
  apply_immediately     = var.apply_immediately
}

data "aws_vpc" "vpc" {
  tags = {
    Name = "${var.product}-${var.environment}"
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.vpc.id
  tags = {
    Name = "*private*"
  }
}

resource "aws_security_group" "rds" {
  name = "${var.product}-${var.environment}-rds"

  vpc_id = data.aws_vpc.vpc.id

  ingress {
    from_port   = local.port
    to_port     = local.port
    protocol    = "tcp"
    self        = true
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.product}-${var.environment}-rds"
    Environemnt = "${var.environment}"
    Product     = "${var.product}"
  }
}

module "master" {
  source  = "terraform-aws-modules/rds/aws"
  version = "2.5.0"

  identifier = "${var.product}-${var.environment}-master"

  # DB basic configuration
  engine                = local.engine
  engine_version        = local.engine_version
  instance_class        = local.instance_class
  allocated_storage     = local.allocated_storage
  max_allocated_storage = local.max_allocated_storage
  storage_encrypted     = local.storage_encrypted
  multi_az              = var.multi_az
  publicly_accessible   = false

  name     = "${var.product}_${var.environment}"
  username = var.db_master_user
  password = "${random_password.password.result}"
  port     = local.port

  # DB option group
  create_db_option_group    = var.multi_az ? false : true
  major_engine_version = var.major_engine_version != "" ? var.major_engine_version : "${lookup(var.config, "${var.engine}_major_engine_version")}"

  # DB parameter group
  create_db_parameter_group = var.multi_az ? false : true
  family = var.family != "" ? var.family : "${var.engine}${lookup(var.config, "${var.engine}_family")}"

  # DB maintenance
  maintenance_window      = var.maintenance_window
  backup_window           = var.backup_window
  backup_retention_period = var.backup_retention_period

  # DB subnet group
  subnet_ids = data.aws_subnet_ids.private.ids

  vpc_security_group_ids = [ aws_security_group.rds.id ]

  # Determines whether a final DB snapshot is created before the DB instance is deleted
  skip_final_snapshot = var.skip_final_snapshot
  final_snapshot_identifier = "${var.product}-${var.environment}"

  # Database Deletion Protection
  deletion_protection = var.deletion_protection

  apply_immediately = local.apply_immediately

  tags = {
    Environment = var.environment
    Product     = var.product
  }

  parameters = var.parameters

  options = var.options
}

resource "aws_db_instance" "replica" {
  count = var.replica_count

  identifier = "${var.product}-${var.environment}-replica-${count.index}"

  # Source database. For cross-region use this_db_instance_arn
  replicate_source_db = module.master.this_db_instance_id

  engine                = local.engine
  engine_version        = local.engine_version
  instance_class        = local.instance_class
  allocated_storage     = local.allocated_storage
  max_allocated_storage = local.max_allocated_storage
  storage_encrypted     = local.storage_encrypted

  multi_az = false

  # Username and password should not be set for replicas
  username = ""
  password = ""
  port     = local.port

  # DB maintenance
  maintenance_window      = var.maintenance_window
  backup_window           = var.backup_window

  # disable backups to create DB faster
  backup_retention_period = 0

  # disable final snapshots on replicas
  skip_final_snapshot = true

  apply_immediately = local.apply_immediately
}

resource "aws_ssm_parameter" "rds_connection_endpoint" {
  name        = "/${var.parameter_store_key_prefix}/connection_endpoint"
  description = "Connection URL for database"
  type        = "String"
  value       = module.master.this_db_instance_endpoint

  tags = {
    Environment = var.environment
    Product     = var.product
    type        = "database connection endpoint"
  }
}

resource "random_password" "password" {
  length = 32
  special = false
  override_special = "/@"
}

resource "aws_ssm_parameter" "password" {
  name        = "/${var.parameter_store_key_prefix}/password"
  description = "The parameter description"
  type        = "SecureString"
  value       = random_password.password.result

  tags = {
    Environment = var.environment
    Product     = var.product
    type        = "database password"
  }
}
