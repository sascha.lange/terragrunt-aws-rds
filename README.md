# RDS

Module to create rds instances. It uses the approved module from the [terrafom registry](https://registry.terraform.io/modules/terraform-aws-modules/rds/aws/2.5.0?tab=inputs)

---

## ToDo

**Priority topics:**

- Generate dynamic password and store in parameter store
- Allow database parameters and options

**Secondary topics:**

- Make DB creation optional
- Verify snapshot archiving, make sure snapshots stay there after db is deleted
- autoscaling storage - double compared to allocated storage
- Allow multi_az enable / disable during runtime
- RDS mysql setup defaults to 5.7, but 8.0 is also possible
- Create module documentation
- Special characters need some work (currently disable)
